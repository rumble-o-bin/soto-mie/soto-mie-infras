storage "s3" {
    access_key = "<secret>"
    secret_key = "<secret>"
    endpoint   = "minio-svc.minio-dev.svc.cluster.local:9090"
    bucket     = "vault"
    s3_force_path_style = "true"
    disable_ssl = "true"
}