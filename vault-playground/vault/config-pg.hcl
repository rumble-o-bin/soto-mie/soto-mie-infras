storage "postgresql" {
  connection_url = "postgres://app_user:test123@vault-postgresql.vault.svc.cluster.local:5432/vault?sslmode=disable"
  max_idle_connections = 2
  table = "vault_kv_store"
  #ha_enabled = true
  #ha_table = "vault_ha_locks"
}
