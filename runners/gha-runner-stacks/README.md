# notes

## gha-runner-scale-set-controller

```bash
helm install arc \
    --namespace arc-system \
    --create-namespace \
    oci://ghcr.io/actions/actions-runner-controller-charts/gha-runner-scale-set-controller
```

## gha-runner-scale-set

```bash
helm install efw-runner \
    --namespace gh-runner \
    --create-namespace \
    -f gha-runner-scale-set/values.yaml
    oci://ghcr.io/actions/actions-runner-controller-charts/gha-runner-scale-set
```

### sekret

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: efw-runner-runner
  namespace: gh-runner
stringData:
  github_app_id: "<redacted>"
  # https://github.com/organizations/<org_name>/settings/installations/<installation_id>
  github_app_installation_id: "<redacted>"
  github_app_private_key: |
    -----BEGIN RSA PRIVATE KEY-----
    <redacted>
    -----END RSA PRIVATE KEY-----

## Sources

- https://devopstales.github.io/home/github-self-hosted-runners-on-kubernetes/
- https://medium.com/google-cloud/github-action-runners-on-gke-with-dind-rootless-bd54e23516c9