#!/bin/bash

for c in $(find . -name "Chart.yaml" -exec dirname {} \;); do
    echo "----------------------------------------"
    echo "📌 Linting check for $c"
    echo "----------------------------------------"

    helm lint "$c"

    check_response=$?

    if [ $check_response -ne 0 ]; then
        echo "Error linting check $c"
        exit 1
    fi
done

echo "End linting check"