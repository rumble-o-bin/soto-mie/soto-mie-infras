from redis.sentinel import Sentinel

# Define the list of sentinel hosts (using the single DNS entry)
sentinel_host = ('redis-sentinel-sentinel-headless.default.svc.cluster.local', 26379)

# Create a Redis Sentinel object
sentinel = Sentinel([sentinel_host], socket_timeout=0.1)

# Specify the name of the master to connect to
master = sentinel.master_for('redis-sentinel-master', socket_timeout=0.1)

# Perform operations on the Redis master
master.set('key', 'value222')
value = master.get('key')
print(value.decode('utf-8'))  # Decode the bytes to string
