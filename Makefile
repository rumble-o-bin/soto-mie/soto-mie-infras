# Drop your variables below
CLUSTER_NAME="kind-infras"
CLUSTER_VERSION="kindest/node:v1.28.15"

.PHONY: help
help:  ## Show help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

start_cluster: ## Start kind cluster
	@kind create cluster --name $(CLUSTER_NAME) --config .hack/config/kind-config.yaml --image $(CLUSTER_VERSION)
	@sleep 1
	@kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.14.5/config/manifests/metallb-native.yaml
	@sleep 30
	@kubectl apply -k .hack/config/

delete_cluster: ## Delete cluster kind
	@kind delete cluster --name $(CLUSTER_NAME)

start_vault: ## Start Vault
	@docker compose -f .hack/vault/docker-compose.yml up -d

stop_vault: ## Stop Vault
	@docker compose -f .hack/vault/docker-compose.yml down

init_vcluster_environment: ## Init vcluster environment
	@vcluster create vcluster-test -f virtual-cluster/overlays/test/values.yaml --connect=false
	@vcluster create vcluster-staging -f virtual-cluster/overlays/test/values.yaml --connect=false

hl_init: ## helmfile init
	@helmfile init -f 02-clusters/kind-infras/helmfile.yaml

hl_diff: ## helmfile diff check
	@helmfile diff --color --context 20 -f 02-clusters/kind-infras/helmfile.yaml

hl_apply: ## helmfile apply
	@HELM_DIFF_THREE_WAY_MERGE=true helmfile apply --color --skip-deps --context 20 -f 02-clusters/kind-infras/helmfile.yaml

hl_sync: ## helmfile sync
	@helmfile sync --color --skip-deps -f 02-clusters/kind-infras/helmfile.yaml

hl_list: ## helmfile list
	@helmfile list -f 02-clusters/kind-infras/helmfile.yaml
